# UAV-BF109

:airplane: The UAV-BF109 project is built at a scale model. It is [Messerschmitt bf109 V2](https://en.wikipedia.org/wiki/Messerschmitt).

- Size E
- 45in bf 109 plan
- Rev D
- Scale 1:1
- Weigth:
- Sheet 2 of 2

The website plans of the [bf109](http://modelaviation.com/bf109-intro "bf109-intro").

## Development of an Unmanned Aerial Vehicle (UAV) 

![alt text](./Assets/freecad-tutorial/UAV-is-in-development.png)


## Freecad Tutorial.

Create new freecad project

![alt text](./Assets/freecad-tutorial/create-new-freecad.png)

Create folder

![alt text](./Assets/freecad-tutorial/create-folder-and-file.png)

Select part design

![alt text](./Assets/freecad-tutorial/select-part-design.png)

Select icon part design

![alt text](./Assets/freecad-tutorial/icon-part-design.png)

Part design

![alt text](./Assets/freecad-tutorial/part-design.png)

Select image

![alt text](./Assets/freecad-tutorial/select-image.png)

Click planar image

![alt text](./Assets/freecad-tutorial/click-planar-image.png)

Select airplane image

![alt text](./Assets/freecad-tutorial/select-airplane-image.png)

Image orientation

![alt text](./Assets/freecad-tutorial/image-orientation.png)

Select Sketch

![alt text](./Assets/freecad-tutorial/select-Sketch.png)

Sketch orientation

![alt text](./Assets/freecad-tutorial/sketch-orientation.png)


